# README #

GKEにCloud Buildでデプロイするための実験

# ローカル起動方法
docker-compose up --build

# デプロイ対象環境構築
gcloud builds submit . --config=cloud_build/init.yaml --substitutions=_GKE_ZONE=us-west2-a,_GKE_CLUSTER_NAME=cluster-xxx,_GKE_MACHINE_TYPE=g1-small
