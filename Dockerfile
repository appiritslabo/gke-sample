FROM golang:1.9-alpine

RUN set -ex \
      && apk add --no-cache --virtual .build-deps git \
      && go get github.com/golang/dep/cmd/dep

WORKDIR $GOPATH/src/app
COPY . $GOPATH/src/app

RUN dep ensure
RUN go build -o bin/app
CMD bin/app

RUN set -ex \
      && apk del .build-deps
